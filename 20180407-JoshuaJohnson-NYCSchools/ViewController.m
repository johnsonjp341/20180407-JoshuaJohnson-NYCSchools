//
//  ViewController.m
//  20180407-JoshuaJohnson-NYCSchools
//
//  Created by Joshua Johnson on 4/7/18.
//  Copyright © 2018 Joshua Johnson. All rights reserved.
//
/*This app displays the SAT data for NYC schools. It uses a UITable to list the schools. Once clicked additional information appears. The app is displaying the number of test takers, all the scores, and the unique database indexing number for each school so that searches can be done across multiple databases that use the same unique number for the corresponding school. I did not have time to connect the second database because I had no notice to clear time for this project this weekend.
 
 The app has all of the school information stored in XML format. Why, well I've mainly been using JSON recently and so I thought a journey down XML lane would be fun. Also, the NYC data page didn't produce a very clean JSON output.
 
 Parsing is done using NSXMLParser. The parsed values are placed into Mutable Arrays for each type of information such as math or writing scores.
 
 Each school then gets a place in a Scrollable UITableView.
 
 Some oddities to note:
 
      Dispatch is used for a loading bar in case we decide to parse a gigantic database in the future or phones get slowed down - because Apple.
 
      NSXMLParser is reliable, but can be slow. There are other options, it is just the native Objective-C solution.
 
      With only a few hours to build this I skipped the unit tests. NOT A GOOD PRACTICE. ADDITIONALLY, WHEN PARSING INFORMATION ESPECIALLY FROM THE WEB IT is important to include encoding filters to separate no-no characters such as the &.
 
      Objective-C. I am open to learning Swift. I just have not got to it yet. Now to see if I will be permitted to submit an Android app too. I'm better at Java.
 
      Pretty much everything important is in ViewController.m . Normally I would not put everything there...
 
       
 
 
 */
#import <Foundation/Foundation.h>
#import "ViewController.h"
#import <UIKit/UIKit.h>


@interface ViewController ()

@end

@implementation ViewController

//Loading bar in case parsing and tables are slow.
UIActivityIndicatorView *indicator;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Friendly console greeting to hiring managers.
    NSLog(@"Hello JP MORGAN");
    
    //Using a single page app for this demo. Need the data parsed to show up in a table.
    [self parseXML];
    
    //init arrays used for query lists.
    
    _schoolList = [[NSMutableArray alloc] init];
    _numberOfTakers = [[NSMutableArray alloc] init];
    _writingScore = [[NSMutableArray alloc] init];
    _mathScore = [[NSMutableArray alloc] init];
    _readingScore = [[NSMutableArray alloc] init];
    _databaseIdentifier = [[NSMutableArray alloc] init];
    _totalStudents = [[NSMutableArray alloc] init];
    
    // Spinning circle loading bar in case loading is slow. That way user does not think we forgot about them.
    
    indicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicator.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    indicator.center = self.view.center;
    indicator.hidesWhenStopped = true;
    [self.view addSubview:indicator];
    [indicator bringSubviewToFront:self.view];
    
    
   
    
    // Use a dispatch to control loading bar in the event database query is delayed.
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),
                   ^ {
                       
                         [self parseXML];
                       
                       dispatch_async(dispatch_get_main_queue(), ^{
                           
                           // The GUI thread stuff goes here
                           [self.tableView reloadData]; // example
                           
                           
                           [indicator stopAnimating];
                           [indicator removeFromSuperview];
                       });
                       
                       
                   });
    

    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Using the good ole NSXMLParser. Not the fastest library, but it works.
-(void)parseXML{

    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"SATScores" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
    
    //At quick glance there are &s mixed in the data. These need to be filtered out so that the Parser does not skip lines. If I had some time over the weekend I would have added more filters to make sure only allowed characters are parsed.
    
    NSString *str = [[NSString alloc] initWithData:xmlData encoding:NSASCIIStringEncoding];
    NSString *contentString = [str stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    xmlData=[contentString dataUsingEncoding:NSUTF8StringEncoding];
    

    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    
    [xmlParser setDelegate:self];
   
    [xmlParser parse];
    
    
    
}

//Second parse method to be called to access more information. On demand saves resources.
-(void)parseAdditionalXML{
    
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"MoreInfo" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:filePath];
    
    //At quick glance there are &s mixed in the data. These need to be filtered out so that the Parser does not skip lines. If I had some time over the weekend I would have added more filters to make sure only allowed characters are parsed.
    
    NSString *str = [[NSString alloc] initWithData:xmlData encoding:NSASCIIStringEncoding];
    NSString *contentString = [str stringByReplacingOccurrencesOfString:@"&" withString:@"and"];
    xmlData=[contentString dataUsingEncoding:NSUTF8StringEncoding];
    
    
    
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    
    [xmlParser setDelegate:self];
    
    [xmlParser parse];
    
    
    
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict{

    self.currentElement=elementName;
    
}

-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName{
    
    self.currentElement=@"";
    
}

// Add the parsed data to separate Mutable arrays.
-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
  
    
    if([self.currentElement isEqualToString:@"school_name"]){
        
       
        
        [_schoolList addObject:string];
        
        
    }
    
    if([self.currentElement isEqualToString:@"num_of_sat_test_takers"]){
        
        
        
        [_numberOfTakers addObject:string];
        
        
    }
    
    if([self.currentElement isEqualToString:@"sat_critical_reading_avg_score"]){
        
        
        
        [_readingScore addObject:string];
        
        
    }
    if([self.currentElement isEqualToString:@"sat_math_avg_score"]){
        
        
        
        [_mathScore addObject:string];
        
        
    }
    
    if([self.currentElement isEqualToString:@"sat_writing_avg_score"]){
        
        
        
        [_writingScore addObject:string];
        
        
    }
    
    if([self.currentElement isEqualToString:@"dbn"]){
        
        //The schools have a database identifier that appears to be consistent across the various databases offered by the NYC site. I will use these numbers to reference another database for additional information.
        
        [_databaseIdentifier addObject:string];
        
        
    }
    
   
    
    if([self.currentElement isEqualToString:@"total_students"]){
        
        //The schools have a database identifier that appears to be consistent across the various databases offered by the NYC site. I will use these numbers to reference another database for additional information.
        
        [_totalStudents addObject:string];
        
        
    }
    
    
    
    
}

// Set the text of the cells in the table.

-(UITableViewCell *)tableView:(UITableView *)tableView
        cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    static NSString *CellIdentifier =@"Cell";
    UITableViewCell *cell = [tableView
                             dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    
     cell.textLabel.text=[_schoolList objectAtIndex:indexPath.row];
    
   
    NSString *CInumberOfTakers = [_numberOfTakers objectAtIndex:indexPath.row];
    NSString *CIreadingScore = [_readingScore objectAtIndex:indexPath.row];
    NSString *CIwritingScore = [_writingScore objectAtIndex:indexPath.row];
    NSString *CImathScore = [_mathScore objectAtIndex:indexPath.row];
    
    NSString *scoreDetails = [NSString stringWithFormat: @"Number of Takers: %@ Reading: %@ Math: %@ Writing: %@", CInumberOfTakers, CIreadingScore, CImathScore, CIwritingScore];
    
    cell.detailTextLabel.numberOfLines = 4;
    cell.detailTextLabel.text= scoreDetails;
    

    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    
    

    //use the count method of the array to provide the correct number of lines for the UITable
    NSInteger arraycount = [_schoolList count];
    
   
    return arraycount -1;
    
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

//If you click me more information will appear.
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedCell = (UITableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    

    
     _identToParse = [_databaseIdentifier objectAtIndex:indexPath.row];
    
    NSString *labelText = [NSString stringWithFormat: @"This is the unique identifier for each school: \n %@ \n I would use it to query information from the set of data with additional school information. Unfortunately, it has been a busy weekend and I do not have the time." , _identToParse];
    
   
   
    _AdditionalInfoText.text = labelText;
    
}

@end
