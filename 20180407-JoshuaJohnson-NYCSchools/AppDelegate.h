//
//  AppDelegate.h
//  20180407-JoshuaJohnson-NYCSchools
//
//  Created by Joshua Johnson on 4/7/18.
//  Copyright © 2018 Joshua Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

