//
//  main.m
//  20180407-JoshuaJohnson-NYCSchools
//
//  Created by Joshua Johnson on 4/7/18.
//  Copyright © 2018 Joshua Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
