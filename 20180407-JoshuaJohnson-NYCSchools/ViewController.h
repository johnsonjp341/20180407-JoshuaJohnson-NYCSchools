//
//  ViewController.h
//  20180407-JoshuaJohnson-NYCSchools
//
//  Created by Joshua Johnson on 4/7/18.
//  Copyright © 2018 Joshua Johnson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <dispatch/dispatch.h>

@interface ViewController : UIViewController <NSXMLParserDelegate>

//Keep an eye on the current element being parsed
@property(retain,nonatomic)NSString *currentElement;

//Mutable arrays to hold specific data
@property (nonatomic,strong) NSMutableArray *schoolList;
@property (nonatomic,strong) NSMutableArray *numberOfTakers;
@property (nonatomic,strong) NSMutableArray *mathScore;
@property (nonatomic,strong) NSMutableArray *readingScore;
@property (nonatomic,strong) NSMutableArray *writingScore;
@property (nonatomic,strong) NSMutableArray *databaseIdentifier;
@property (nonatomic,strong) NSMutableArray *totalStudents;
@property (nonatomic,strong) NSString *identToParse;

//Using UITable to display information. This is inherited from scroll view.
@property(nonatomic, strong) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UILabel *AdditionalInfoText;
@property (nonatomic,strong) UITableViewCell *selectedCell;


@end

